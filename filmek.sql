USE [master]
GO
/****** Object:  Database [Beadando]    Script Date: 2022. 11. 24. 0:38:46 ******/
CREATE DATABASE [Beadando]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Beadando', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\Beadando.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Beadando_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\Beadando_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [Beadando] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Beadando].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Beadando] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Beadando] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Beadando] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Beadando] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Beadando] SET ARITHABORT OFF 
GO
ALTER DATABASE [Beadando] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Beadando] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Beadando] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Beadando] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Beadando] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Beadando] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Beadando] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Beadando] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Beadando] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Beadando] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Beadando] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Beadando] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Beadando] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Beadando] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Beadando] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Beadando] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Beadando] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Beadando] SET RECOVERY FULL 
GO
ALTER DATABASE [Beadando] SET  MULTI_USER 
GO
ALTER DATABASE [Beadando] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Beadando] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Beadando] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Beadando] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Beadando] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Beadando] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Beadando', N'ON'
GO
ALTER DATABASE [Beadando] SET QUERY_STORE = ON
GO
ALTER DATABASE [Beadando] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [Beadando]
GO
/****** Object:  Table [dbo].[Filmek]    Script Date: 2022. 11. 24. 0:38:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Filmek](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Film_Cim] [nchar](200) NULL,
	[Film_Mufaj] [nchar](200) NULL,
	[IMDB] [int] NULL,
	[Film_Datum] [nchar](200) NULL,
 CONSTRAINT [PK_Filmek] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rendezo]    Script Date: 2022. 11. 24. 0:38:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rendezo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Rendezo_Neve] [nchar](200) NULL,
	[Rendezett_Film] [nchar](200) NULL,
 CONSTRAINT [PK_Rendezo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Szinesz]    Script Date: 2022. 11. 24. 0:38:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Szinesz](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Szinesz_neve] [nchar](200) NULL,
	[Szinesz_kora] [int] NULL,
	[Szerepelt_film] [nchar](200) NULL,
 CONSTRAINT [PK_Szinesz] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Filmek] ON 

INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (1, N'A mestergyilkos                                                                                                                                                                                         ', N'Akció                                                                                                                                                                                                   ', 6, N'2011                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (2, N'A szállító                                                                                                                                                                                              ', N'Akció                                                                                                                                                                                                   ', 7, N'2002                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (3, N'Üveg                                                                                                                                                                                                    ', N'Szuperhős-thriller                                                                                                                                                                                      ', 7, N'2019                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (4, N'Széttörve                                                                                                                                                                                               ', N'Horrorfilm                                                                                                                                                                                              ', 7, N'2016                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (5, N'A sebezhetetlen                                                                                                                                                                                         ', N'Misztikus thriller                                                                                                                                                                                      ', 7, N'2000                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (6, N'A könyvelő                                                                                                                                                                                              ', N'Akció                                                                                                                                                                                                   ', 8, N'2016                                                                                                                                                                                                    ')
INSERT [dbo].[Filmek] ([Id], [Film_Cim], [Film_Mufaj], [IMDB], [Film_Datum]) VALUES (13, N'A védelmező 2                                                                                                                                                                                           ', N'Akció                                                                                                                                                                                                   ', 7, N'2018                                                                                                                                                                                                    ')
SET IDENTITY_INSERT [dbo].[Filmek] OFF
GO
SET IDENTITY_INSERT [dbo].[Rendezo] ON 

INSERT [dbo].[Rendezo] ([Id], [Rendezo_Neve], [Rendezett_Film]) VALUES (1, N'Simon West                                                                                                                                                                                              ', N'A mestergyilkos                                                                                                                                                                                         ')
INSERT [dbo].[Rendezo] ([Id], [Rendezo_Neve], [Rendezett_Film]) VALUES (2, N'Louis Leterrier                                                                                                                                                                                         ', N'A szállító                                                                                                                                                                                              ')
INSERT [dbo].[Rendezo] ([Id], [Rendezo_Neve], [Rendezett_Film]) VALUES (3, N'Corey Yuen                                                                                                                                                                                              ', N'A szállító                                                                                                                                                                                              ')
INSERT [dbo].[Rendezo] ([Id], [Rendezo_Neve], [Rendezett_Film]) VALUES (4, N'M. Night Shyamalan                                                                                                                                                                                      ', N'Üveg, Széttörve, A sebezhetetlen                                                                                                                                                                        ')
INSERT [dbo].[Rendezo] ([Id], [Rendezo_Neve], [Rendezett_Film]) VALUES (5, N'Gavin O’Connor                                                                                                                                                                                          ', N'A könyvelő                                                                                                                                                                                              ')
SET IDENTITY_INSERT [dbo].[Rendezo] OFF
GO
SET IDENTITY_INSERT [dbo].[Szinesz] ON 

INSERT [dbo].[Szinesz] ([Id], [Szinesz_neve], [Szinesz_kora], [Szerepelt_film]) VALUES (1, N'Jason Statham                                                                                                                                                                                           ', 55, N'A mestergyilkos, A szállító                                                                                                                                                                             ')
INSERT [dbo].[Szinesz] ([Id], [Szinesz_neve], [Szinesz_kora], [Szerepelt_film]) VALUES (2, N'James McAvoy                                                                                                                                                                                            ', 43, N'Üveg, Széttörve                                                                                                                                                                                         ')
INSERT [dbo].[Szinesz] ([Id], [Szinesz_neve], [Szinesz_kora], [Szerepelt_film]) VALUES (3, N'Bruce Willis                                                                                                                                                                                            ', 67, N'A sebezhetetlen                                                                                                                                                                                         ')
INSERT [dbo].[Szinesz] ([Id], [Szinesz_neve], [Szinesz_kora], [Szerepelt_film]) VALUES (4, N'Ben Affleck                                                                                                                                                                                             ', 50, N'A könyvelő                                                                                                                                                                                              ')
SET IDENTITY_INSERT [dbo].[Szinesz] OFF
GO
USE [master]
GO
ALTER DATABASE [Beadando] SET  READ_WRITE 
GO
