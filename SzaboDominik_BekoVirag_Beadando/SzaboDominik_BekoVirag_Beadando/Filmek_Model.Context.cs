﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SzaboDominik_BekoVirag_Beadando
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BeadandoEntities : DbContext
    {
        public BeadandoEntities()
            : base("name=BeadandoEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Film> Filmek { get; set; }
        public virtual DbSet<Rendezo> Rendezok { get; set; }
        public virtual DbSet<Szinesz> Szineszek { get; set; }
    }
}
