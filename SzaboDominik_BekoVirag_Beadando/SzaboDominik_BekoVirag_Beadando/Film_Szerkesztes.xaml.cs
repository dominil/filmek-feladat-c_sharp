﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SzaboDominik_BekoVirag_Beadando
{
 
    public partial class Film_Szerkesztes : Window
    {

        // Adatbázis kapcsolat létrehozása
        public static string conString = "Data Source=DOMINIK-PC;Initial Catalog=Beadando;Integrated Security=True";
        public SqlConnection con = new SqlConnection(conString);
        public static DataGrid dataGrid;

        public Film_Szerkesztes()
        {
            InitializeComponent();
        }

       
        public void ellenorzes()
        {
            // Szöveges mezők ellenőrzése (üres e)

            // Megnézi üres e a film id mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(this.film_id.Text))
            {
                MessageBox.Show("Üres az id mező!");
                return;
            }

            // Megnézi üres e a film cim mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(this.film_cim.Text)) 
            {
                MessageBox.Show("Üres a film cím mező");
                return;
            }

            // Megnézi üres e a műfaj mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(film_mufaj.Text))
            {
                MessageBox.Show("Hiányzik a film műfaja!");
                return;
            }

            // megnézi üres e az imdb mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(imdb.Text))
            {
                MessageBox.Show("Hiányzik az IMDB értékelés!");
                return;
            }

            // megnézi üres e az dátum mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(film_datum.Text))
            {
                MessageBox.Show("Hiányzik a film dátum!");
                return;
            }
        }

        // film szerkesztés gomb megnyomásakor meghívott függvény
        private void film_szerkesztes(object sender, RoutedEventArgs e)
        {
            // Ellenrőzi a mezőket
            ellenorzes();

            try
            {
                // megnyitja a kapcsolatot az adatbázissal
                con.Open();

                // Frissiti az adatbázis a mezőkbe beírt értékekkel
                string query ="update Filmek set Film_Cim=@filmCim, Film_Mufaj=@filmMufaj, IMDB=@imdb, Film_Datum=@filmDatum where Id=@Id"; 
                
                // sql parancs átadasa 
                SqlCommand cmd = new SqlCommand(query, con);

                // paraméterek alkalmazása
                cmd.Parameters.AddWithValue("@filmCim", film_cim.Text);
                cmd.Parameters.AddWithValue("@filmMufaj", film_mufaj.Text);
                cmd.Parameters.AddWithValue("@imdb", imdb.Text);
                cmd.Parameters.AddWithValue("@filmDatum", film_datum.Text);
                cmd.Parameters.AddWithValue("@Id", int.Parse(film_id.Text));

                // sql parancs végre hajtása
                cmd.ExecuteNonQuery();

                // kapcsolat bezárása
                con.Close();

                MessageBox.Show("Sikeres szerkesztés!");
                
            }
            catch (Exception)
            {
                MessageBox.Show("Sikertelen szerkesztés");
            }
        }
    }
}
