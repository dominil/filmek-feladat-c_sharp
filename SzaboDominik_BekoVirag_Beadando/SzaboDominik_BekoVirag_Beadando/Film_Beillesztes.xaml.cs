﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SzaboDominik_BekoVirag_Beadando
{
    /// <summary>
    /// Interaction logic for Film_Beillesztes.xaml
    /// </summary>
    public partial class Film_Beillesztes : Window
    {
        // adatbázis kapcsolat létrehozása
        BeadandoEntities db = new BeadandoEntities();

        public Film_Beillesztes()
        {
            InitializeComponent();
        }

        // film beillesztés gomba katintva meghívott függvény
        private void film_beillesztes(object sender, RoutedEventArgs e)
        {

            // Szöveges mezők ellenőrzése

            // Megnézi üres e a film cim mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(film_cim.Text)) 
            { 
                MessageBox.Show("Hiányzik a film címe!");
                return;
            }

            // Megnézi szerepel e már ugyan az a film az adatbázisban amit beakarunk illeszteni ha igen küld egy hiba üzenetet
            if (db.Filmek.Any(n => n.Film_Cim == film_cim.Text)) 
            { 
                MessageBox.Show($"{film_cim.Text} film már szerepel az adatbázisban!");
                return;
            }

            // Megnézi üres e a műfaj mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(film_mufaj.Text))
            {
                MessageBox.Show("Hiányzik a film műfaja!");
                return;
            }

            // megnézi üres e az imdb mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(imdb.Text))
            {
                MessageBox.Show("Hiányzik az IMDB értékelés!");
                return;
            }

            // megnézi üres e az dátum mező ha igen küld egy hiba üzenetet
            if (string.IsNullOrEmpty(film_datum.Text))
            {
                MessageBox.Show("Hiányzik a film dátum!");
                return;
            }

            // hozzá ad a filmek adatbzáis egy új filmet, és beállítja a következő értékeket
            db.Filmek.Add(new Film {Film_Cim = film_cim.Text, Film_Mufaj = film_mufaj.Text, IMDB = Convert.ToInt32(imdb.Text), Film_Datum = film_datum.Text });
            MessageBox.Show("Sikeres film hozzáadás");

            // adatbázis változtatások mentése
            db.SaveChanges();
        }

    }
}
