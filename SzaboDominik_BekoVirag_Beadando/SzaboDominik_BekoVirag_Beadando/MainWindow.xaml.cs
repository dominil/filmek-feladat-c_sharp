﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SzaboDominik_BekoVirag_Beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // adatbázis kapcsolat létrehozása
        BeadandoEntities db = new BeadandoEntities();
        public static string conString = "Data Source=DOMINIK-PC;Initial Catalog=Beadando;Integrated Security=True";
        public SqlConnection con = new SqlConnection(conString);
        public static DataGrid dataGrid;

        public MainWindow()
        {
            InitializeComponent();
        }


        // alkalmazásból kilépés
        private void kilepes(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        // Összes film megjelenítése
        private void Osszes_Film(object sender, RoutedEventArgs e)
        {
            listazas.ItemsSource = db.Filmek.ToList();
            dataGrid = listazas;
        }

        // Összes színész megjelenítése
        private void Osszes_Szinesz(object sender, RoutedEventArgs e)
        {
            listazas.ItemsSource = db.Szineszek.ToList();
            dataGrid = listazas;
        }

        // Összes rendező megjelenítése
        private void Osszes_rendezo(object sender, RoutedEventArgs e)
        {
            listazas.ItemsSource = db.Rendezok.ToList();
            dataGrid = listazas;
        }

        // film beilesztés ablak meghívása a film beillesztés gombra kattintva 
        private void film_Beillesztes_ablak(object sender, RoutedEventArgs e)
        {
            Film_Beillesztes film_beillesztes_ablak = new Film_Beillesztes();
            film_beillesztes_ablak.Show();
        }

        // film törlés ablak meghívása a film törlés gombra kattintva
        private void film_törlés_ablak(object sender, RoutedEventArgs e)
        {
            Film_Törlés film_Törlés_ablak = new Film_Törlés();
            film_Törlés_ablak.Show();
        }

        // film szerkesztés ablak meghívása a film szerkesztés gombra kattintva
        private void film_szerkesztes_ablak(object sender, RoutedEventArgs e)
        {
            Film_Szerkesztes film_Szerkesztes_ablak = new Film_Szerkesztes();
            film_Szerkesztes_ablak.Show();
        }

        private void keresés(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Ez a funkció nem müködik");
        }
    }
}