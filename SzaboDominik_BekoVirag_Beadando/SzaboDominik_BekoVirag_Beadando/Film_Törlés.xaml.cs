﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SzaboDominik_BekoVirag_Beadando
{
    /// <summary>
    /// Interaction logic for Film_Törlés.xaml
    /// </summary>
    public partial class Film_Törlés : Window
    {
        // Adatbázis kapcsolat létrehozásacon
        public static string conString = "Data Source=DOMINIK-PC;Initial Catalog=Beadando;Integrated Security=True";
        public SqlConnection con = new SqlConnection(conString);

        public Film_Törlés()
        {
            InitializeComponent();
        }

        // film törlése gomb megnyomásakor meghívott függvény
        private void film_Törlés(object sender, RoutedEventArgs e)
        {
            // Megnézi üres e a film cim mező
            if (string.IsNullOrEmpty(this.filmCim.Text)) 
            { 
                MessageBox.Show("Üres a film cím mező!"); 
                return; 
            }

            
            try
            {
                // megnyitja a kapcsolatot az adatbázissal
                con.Open();

                // Kitörli az adatbázisból az adatokat a mezőbe beírt id alapján
                string query = $"DELETE FROM filmek " +
                    $"WHERE Film_Cim='{this.filmCim.Text}'";

                // sql parancs átadasa 
                SqlCommand cmd = new SqlCommand(query, con);

                // sql parancs végre hajtása
                cmd.ExecuteNonQuery();

                // kapcsolat bezárása
                MessageBox.Show("Sikeres törlés");
                con.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Sikertelen törlés");
            }

        }
    }
}
